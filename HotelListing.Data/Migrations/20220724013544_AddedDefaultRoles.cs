﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HotelListing.API.Migrations
{
    public partial class AddedDefaultRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "2f881f05-2aff-489c-af83-f0ec02c714e5", "8f3fde37-70fb-4d18-a714-bda3ac09ce33", "Administrator", "ADMINISTRATOR" },
                    { "30ebb958-384d-4a85-add4-b687dff24a65", "068e65d9-fe87-44f0-8507-fa2fe53428e3", "User", "USER" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2f881f05-2aff-489c-af83-f0ec02c714e5");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "30ebb958-384d-4a85-add4-b687dff24a65");
        }
    }
}
