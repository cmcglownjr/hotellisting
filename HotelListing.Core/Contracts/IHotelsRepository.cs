using HotelListing.API.Data;

namespace HotelListing.Core.Contracts;

public interface IHotelsRepository : IGenericRepository<Hotel>
{
    
}