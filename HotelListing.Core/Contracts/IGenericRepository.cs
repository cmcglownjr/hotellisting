using HotelListing.Core.Models;

namespace HotelListing.Core.Contracts;

public interface IGenericRepository<T> where T : class
{
    //TODO: Change <T> to <TEntity>
    Task<T> GetAsync(int? id);
    Task<List<T>> GetAllAsync();
    Task<PagedResult<TResult>> GetAllAsync<TResult>(QueryParameters queryParameters);
    Task<T> AddAsync(T entity);
    Task DeleteAsync(int id);
    Task UpdateAsync(T entity);
    Task<bool> Exists(int id);
}