using AutoMapper;
using HotelListing.API.Data;
using HotelListing.Core.Contracts;

namespace HotelListing.Core.Repository;

public class HotelsRepository : GenericRepository<Hotel>, IHotelsRepository
{
    public HotelsRepository(HotelListingDbContext context, IMapper mapper) : base(context, mapper)
    {
        
    }
}